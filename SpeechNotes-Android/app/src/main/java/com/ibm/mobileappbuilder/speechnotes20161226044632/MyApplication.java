

package com.ibm.mobileappbuilder.speechnotes20161226044632;

import android.app.Application;
import ibmmobileappbuilder.injectors.ApplicationInjector;
import android.support.multidex.MultiDexApplication;
import ibmmobileappbuilder.cloudant.factory.CloudantDatabaseSyncerFactory;
import java.net.URI;


/**
 * You can use this as a global place to keep application-level resources
 * such as singletons, services, etc.
 */
public class MyApplication extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationInjector.setApplicationContext(this);
        //Syncing cloudant ds
        CloudantDatabaseSyncerFactory.instanceFor(
            "data_notes",
            URI.create("https://9dc8f593-270a-4d99-b74f-080ee2bca081-bluemix:4dffc9b0b1297a7f102740556e5eb9b62752671b7fd37e400a1845b1a0411fd3@9dc8f593-270a-4d99-b74f-080ee2bca081-bluemix.cloudant.com/data_notes")
        ).sync(null);
      }
}
